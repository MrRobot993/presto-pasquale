//owl carousel

$('.partners').owlCarousel({
    loop:false,
    margin:20,
    nav:false,
    autoWidth: false,
    responsiveClass: true,
    responsive:{
        0:{
            items:1
        },
        600:{
            items:3
        },
        1000:{
            items:5
        }
    }
})


// auto-focus modale registrazione

$('#signInModal').on('shown.bs.modal', function () {
    $('#signInEmaiInput').trigger('focus')
})

// menu

let navbar = document.getElementById('navbar')
let logo = document.querySelector('#logo')
let logoName = document.querySelector('.title-name')
let navLinks = document.querySelectorAll('.nav-link')

document.addEventListener("scroll", () =>{
    let scrolled = pageYOffset      
    if(window.innerWidth > 1000){
        if (scrolled > 5) {
            logo.style.width = '50px'
            logoName.classList.add('d-none')
            navbar.classList.remove('py-4')
            navLinks.forEach(element => {
                element.classList.add('d-none')
            });
        }else{
            logo.style.width = '80px'
            logoName.classList.remove('d-none')
            navLinks.forEach(element => {
                element.classList.remove('d-none')
            });
        }
    }
})

// particelle in mobile

if (window.innerWidth > 1000) {
    particlesJS.load('particles-js', './particles.json', function() {
    });
}

// filtro range

let sliderRange = document.querySelector('#filterControlRange')
let output = document.querySelector('#priceRange')
output.innerHTML = sliderRange.value + "€"

sliderRange.oninput = function () {
    output.innerHTML = this.value + "€";
}

//cuori

let loved = document.querySelectorAll('.loved-item')

console.log(loved);

loved.forEach((heart) =>{
    heart.addEventListener('click', () => {
    if (heart.classList.contains('far')) {
        heart.classList.remove('far')
        heart.classList.add('fas')
        heart.classList.add('text-danger')
    } else {
        heart.classList.remove('fas')
        heart.classList.add('far')
        heart.classList.remove('text-danger')
    }
})
})


// filtri in mobile

let filterBar = document.querySelector('#filterBar')

if (window.innerWidth < 1000) {
    filterBar.classList.add('collapse')
}
